import React,{useState} from "react";

const ProgressBar = () => {
  const [bgcolor] = useState('green')

// const ProgressBar = (props) => {
//     const {bgcolor} = props


const containerStyles = {
    height: 20,
    width: "100%",
    backgroundColor: "#e0e0de",
    borderRadius: 50,
    margin: 0,
  };

  const fillerStyles = {
    height: "100%",
    width: "20%",
    backgroundColor: bgcolor,
    borderRadius: "inherit",
    textAlign: "center",
  };

  const labelStyles = {
    padding: 0,
    color: "white",
    fontWeight: "bold",
  };

  return (
    <div style={containerStyles}>
      <div style={fillerStyles}>
        <span style={labelStyles}>{"20/100"}</span>
      </div>
    </div>
  );
};

export default ProgressBar;
