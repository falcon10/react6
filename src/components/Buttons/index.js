import React, {useContext, useState } from "react";
import TableRow from '../TableRow'

function ButtonIncrement(props) {

   return (
     <button style={{ marginLeft: '.5rem'}} onClick={props.onClickFunc}>
     +1
     </button>
   )
}

export default ButtonIncrement
