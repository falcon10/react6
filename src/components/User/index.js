import Style from './style.css';
import {useContext, useEffect, useState} from 'react';
import {CharStats} from '../../context'
import TableRow from "../TableRow";
import ProgressBar from '../../components/ProgressBar'



const User = () => {
  const { name, str, hp, speed, addStr, addName, dmg, lvl } = useContext(CharStats);
  const myStats = {name, str, hp, speed, dmg, lvl};
  // useEffect(()=>{addName(localStorage.getItem('name'))},[])
  // const [getName] = useState(localStorage.clear('name'));
  return (
    <div className="col-6">
     <TableRow {...myStats} />
    </div>
  )
}

export default User;
