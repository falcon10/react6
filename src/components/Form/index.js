import React, {useContext, useState } from "react";
import {GameState} from '../../context'

const Form = () => {
  const [name, addName] = useState(localStorage.getItem('name'));
  // const [getName] = useState(localStorage.removeItem('name'));
  const {isName, setName} = useContext(GameState);
  const handleSubmit = (e) => {
    e.preventDefault();
    setName(true)
    localStorage.setItem('name', name)
  }

 return (
   <form onSubmit = {e => handleSubmit (e)}>
   <label> imię:</label>
      <input name="name" type="text" value={name} onChange={(e)=>addName(e.target.value)}/>
      <input name="send" type="submit" />
   </form>
 )
}

export default Form
